const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const favicon = require('serve-favicon');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const logger = require('morgan');
const config = require('./config');

const app = express();

app.enable('trust proxy');
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(express.static('public'));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
const sessionStore = new MySQLStore({
    host: config.dbHost,
    port: 3306,
    user: config.dbUser,
    password: config.dbPassword,
    database: config.dbName,
    expiration: 86400000
});
app.use(session({proxy: true, secret: config.secret, resave: false, rolling: true, saveUninitialized: false, store: sessionStore, cookie:{maxAge: 86400000, secure: config.secure}}));

app.use('/api', require('./api'));

app.get('/*', (req, res, next) => {
    res.sendFile(__dirname + '/views/index.html');
});

app.listen(config.port, () => console.log("Listening!"));
