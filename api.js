const express = require('express');
const router = express.Router();
const snekfetch = require('snekfetch');
const config = require('./config.js')
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: config.dbHost,
  user: config.dbUser,
  password: config.dbPassword,
  database: config.dbName,
  insecureAuth: true
});
connection.connect();

const staff = ["95579865788456960"]; // Temporary until we have the bot set staff to the database

const query = (q) => {
  return new Promise((resolve, reject) => {
    connection.query(q, (err, res) => {
      if (err) return reject(err);
      resolve(res);
    });
  });
}

router.get('/', (req, res, next) => {
  res.status(420).end();
});

router.get('/news', (req, res, next) => {
  connection.query('SELECT id, title, description, pic FROM news ORDER BY date DESC LIMIT 10', (error, results, fields) => {
    if (error) res.status(500).end();
    res.status(200).json(results);
  });
});

router.get('/news/:id', (req, res, next) => {
  connection.query(`SELECT * FROM news WHERE id=${connection.escape(req.params.id)}`, (error, results, fields) => {
    if (error) res.status(500).end();
    if (results.length == 0) return res.status(404).end();
    res.status(200).json(results[0]);
  });
});

router.get('/botvate/leaderboard', (req, res, next) => {
  connection.query('SELECT Username, Level, XPinLevel FROM experience ORDER BY TotalXP DESC LIMIT 100', (error, results, fields) => {
    if (error) res.status(500).end();
    res.status(200).json(results);
  });
});

router.get('/ascension/release', (req, res, next) => {
  connection.query("SELECT * FROM sitesettings WHERE k='latestRelease'", (error, result) => {
    if (error) res.status(500).end();
    res.status(200).send(result[0].v);
  })
});

router.post('/ascension/submit', (req, res, next) => {
  const form = req.body;
  if (!form.email || !form.artists || !form.title || !form.genre || !form.download) return res.status(400).end();
  if (!form.notes) form.notes = "";
  query(`INSERT INTO ascform (email, artists, title, genre, download, notes) VALUES(${connection.escape(form.email)}, ${connection.escape(form.artists)}, ${connection.escape(form.title)}, ${connection.escape(form.genre)}, ${connection.escape(form.download)}, ${connection.escape(form.notes)});`)
  .then(() => {
    return res.status(200).end();
  }).catch(() => {
    return res.status(500).end();
  });
});

router.get('/triumphant/winner', (req, res, next) => {
  connection.query("SELECT * FROM sitesettings WHERE k='triArt' OR k='triArtist' OR k='triTitle' OR k='triscid'", (error, results) => {
    if (error) res.status(500).end();
    const tri = {};
    for (const setting of results) {
      tri[setting.k] = setting.v;
    }
    res.status(200).json(tri);
  })
});

router.post('/triumphant/submit', (req, res, next) => {
  if (!req.session.loggedin) return res.status(403).end();
  const form = req.body;
  if (!form.artists || !form.title || !form.date || !form.soundcloud || !form.agreement) return res.status(400).end();
  query(`INSERT INTO triform (artists, title, date, soundcloud, user) VALUES(${connection.escape(form.artists)}, ${connection.escape(form.title)}, date ${connection.escape(form.date)}, ${connection.escape(form.soundcloud)}, ${connection.escape(req.session.user.id)});`)
  .then(() => {
    return res.status(200).end();
  }).catch(() => {
    return res.status(500).end();
  });
});

router.get('/staff/*', (req, res, next) => {
  if (!req.session.loggedin || !req.session.isStaff) return res.status(403).end();
  next();
});

router.get('/staff/currentSettings', (req, res, next) => {
  connection.query('SELECT * FROM sitesettings', (error, results) => {
    if (error) {
      console.error(error);
      return res.status(500).end();
    }
    const settings = {};
    for (const setting of results) {
      settings[setting.k] = setting.v;
    }
    return res.status(200).json(settings);
  });
});

router.post('/staff/saveSetting', (req, res, next) => {
  connection.query(`UPDATE sitesettings SET v=${connection.escape(req.body.value)} WHERE k=${connection.escape(req.body.key)}`, (error, response) => {
    if (error) return res.status(500).end();
    res.status(200).end();
  });
});

router.get('/auth/login', (req, res, next) => {
  res.redirect(`https://discordapp.com/api/oauth2/authorize?response_type=code&client_id=${config.clientID}&scope=identify&redirect_uri=${encodeURIComponent(config.redirectURL)}`);
});

router.get('/auth/callback', async (req, res, next) => {
  try {
    if (req.query.error) return res.status(400).send(`Error: ${req.query.error}`);
    if (!req.query.code) return res.status(400).send(`Error: No code`);
    const code = req.query.code;
    const tres = await snekfetch.post('https://discordapp.com/api/oauth2/token').set('Content-Type', 'application/x-www-form-urlencoded').send({
      client_id: config.clientID,
      client_secret: config.clientSecret,
      grant_type: 'authorization_code',
      code,
      redirect_uri: config.redirectURL
    });
    const accessToken = tres.body.access_token;
    const refreshToken = tres.body.refresh_token;
    const userres = await snekfetch.get('https://discordapp.com/api/users/@me').set('Authorization', `Bearer ${accessToken}`);
    const user = userres.body;
    query(`UPDATE discorduser SET Username=${connection.escape(user.username)}, Avatar=${connection.escape(user.avatar)}, Discrim=${connection.escape(user.discriminator)}, AccessToken=${connection.escape(accessToken)}, RefreshToken=${connection.escape(refreshToken)} WHERE UserID=${connection.escape(user.id)}`).then(results => {
      if (results.affectedRows == 0) {
        return query(`INSERT INTO discorduser (UserID, Username, Avatar, Discrim, AccessToken, RefreshToken) VALUES(${connection.escape(user.id)}, ${connection.escape(user.username)}, ${connection.escape(user.avatar)}, ${connection.escape(user.discriminator)}, ${connection.escape(accessToken)}, ${connection.escape(refreshToken)});`)
      } else {
        return;
      }
    }).then(() => {
      req.session.loggedin = true;
      req.session.user = user;
      req.session.isStaff = staff.includes(user.id);
      return res.status(200).send("If this window didn't close automatically, you may close it now. <script>window.close()</script>");
    });
  } catch (e) {
    return res.status(500).send(`Error: ${e.message} ${JSON.stringify(e.body)}`);
  }
});

router.get('/auth/logout', (req, res, next) => {
  req.session.destroy((err) => {
    if (err) return res.status(500).end();
    res.clearCookie('connect.sid', {secure: config.secure});
    return res.status(200).end();
  });
});

router.get('/auth/session', (req, res, next) => {
  const session = {};
  session.loggedin = req.session.loggedin || false;
  session.user = req.session.user;
  session.isStaff = req.session.isStaff || false;
  return res.status(200).json(session);
});

router.all('/*', (req, res, next) => {
  res.status(404).end();
});

module.exports = router;
